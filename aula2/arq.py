#!/usr/bin/python3
#leitura e escrita
#arquivo = open('teste.txt','r')
#print(arquivo.read())
#arquivo.close()

#modo escrita
with open('teste.txt','w') as arquivo:
    letras = [ chr(x)+'\n' for x in range(97, 123)]
    arquivo.writelines(letras)

#modo leitura
##with open('teste.txt','r') as arquivo:
   # print(arquivo.read())

