a = lambda x,y: x+y

print(a(1,2))


nomes = ['thiago','gonzalez']
result = map(lambda x: x.title(), nomes)
print(list(result))

quadrados = map(lambda x: x**2, range(1,11))
print(list(quadrados))

quadrados = [lambda x: x**2 for x in range(1,11)]
print(list(quadrados))


quadrados = []
for x in range (1,11):
    quadrados.append((lambda y:y**2))
    
print(list(quadrados))